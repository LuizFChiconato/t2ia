#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <string>
#include <bits/stdc++.h>
#include "heuristica.hpp"
using namespace std;
extern "C" {
    #include "campo.h"
}

#define MAXSTR 512
#define MAXINT 16

typedef struct JogadaT {
    string campo; 
    char lado_meu;
    int tam_campo;
    vector<JogadaT*> jogadas_possiveis;
} JogadaT;

JogadaT* leJogada (char buf[]) {
    JogadaT* jogada = new JogadaT;

    sscanf(strtok(buf, " \n"), "%c", &(jogada->lado_meu));
    sscanf(strtok(NULL, " \n"), "%d", &(jogada->tam_campo));

    char campo[jogada->tam_campo];
    sscanf(strtok(NULL, " \n"), "%s", campo);
    jogada->campo = campo;

    return jogada;
}

vector<string> geraJogadasPossiveis (JogadaT *jogada) {
    vector<string> jogadas_possiveis;

    string jogada_to_insert = "";
    string campo = "";
    campo.append(jogada->campo);
    // jogadas com filósofos
    int i = 0;
    for (i = 0; i < jogada->tam_campo; i++) {
        if (campo[i] == '.') {
            jogada_to_insert = "";
            jogada_to_insert.append(1, jogada->lado_meu);
            jogada_to_insert.append(" f ");
            jogada_to_insert.append(to_string(i+1));
            jogadas_possiveis.push_back(jogada_to_insert);
        }
    }
    // jogadas com bolas
    int index_bola = 0;
    for (i = 0; i < jogada->tam_campo - 1; i++) {
        if (campo[i] == 'o') {
            index_bola = i;
        }
    }

    // para a direita
    bool fim_saltos = false;
    int index = index_bola;

    vector<int> saltos_possiveis_direita;

    while (!fim_saltos) {
        fflush(stdout);
        if (index > jogada->tam_campo - 1) {
            if (campo[index-1] == 'f') {
                saltos_possiveis_direita.push_back(index);
            }
            fim_saltos = true;
        } else if (campo[index] == '.' && (campo[index-1] == '.' || campo[index-1] == 'o')) {
            fim_saltos = true;
        } else if (campo[index] == '.' && campo[index-1] == 'f') {
            saltos_possiveis_direita.push_back(index);
        }
        index++;
    }

    int j = 0;
    for (i = 0; i < (int) saltos_possiveis_direita.size(); i++) {
        jogada_to_insert = jogada->lado_meu;
        jogada_to_insert.append(" o ");
        jogada_to_insert.append(to_string(i+1));
        jogada_to_insert.append(" ");
        for (j = 0; j <= i; j++) {
            jogada_to_insert.append(to_string(saltos_possiveis_direita[j] + 1));
            jogada_to_insert.append(" ");
        }
        jogadas_possiveis.push_back(jogada_to_insert);
    }

    // para a esquerda
    fim_saltos = false;
    index = index_bola;

    vector<int> saltos_possiveis_esquerda;

    while (!fim_saltos) {
        if (index < 0) {
            if (campo[index+1] == 'f') {
                saltos_possiveis_esquerda.push_back(index);
            }
            fim_saltos = true;
        }  else if (campo[index] == '.' && (campo[index+1] == '.' || campo[index+1] == 'o')) {
            fim_saltos = true;
        } else if (campo[index] == '.' && campo[index+1] == 'f') {
            saltos_possiveis_esquerda.push_back(index);
        }
        index--;
    }

    j = 0;
    for (i = 0; i < (int) saltos_possiveis_esquerda.size(); i++) {
        jogada_to_insert = jogada->lado_meu;
        jogada_to_insert.append(" o ");
        jogada_to_insert.append(to_string(i+1));
        jogada_to_insert.append(" ");
        for (j = 0; j <= i; j++) {
            jogada_to_insert.append(to_string(saltos_possiveis_esquerda[j] + 1));
            jogada_to_insert.append(" ");
        }
        jogadas_possiveis.push_back(jogada_to_insert);
    }

    // jogada nula
    jogada_to_insert = "";
    jogada_to_insert.append(1, jogada->lado_meu);
    jogada_to_insert.append(" n");
    jogadas_possiveis.push_back(jogada_to_insert);


    return jogadas_possiveis;
}

string atualizaCampo (string campo, string jogada_possivel) {
    stringstream jogada_stream(jogada_possivel);
    char lado;
    jogada_stream >> lado;

    char movimento;
    jogada_stream >> movimento;

    int posicao_filosofo;
    int num_saltos;
    vector<int> posicao_bola;

    if (movimento != 'n') {
        if (movimento == 'f') {
            jogada_stream >> posicao_filosofo;
        } else {
            jogada_stream >> num_saltos;
            for (int i = 0; i < num_saltos; i++) {
                int posicao_bola_i;
                jogada_stream >> posicao_bola_i;
                posicao_bola.push_back(posicao_bola_i);
            }
        }
    }

    string campo_aux = "";
    campo_aux.append(campo);
    if (movimento == 'f') {
        campo_aux[posicao_filosofo - 1] = 'f';
    }
    else if (movimento == 'o') {
        int posicao_bola_original = campo_aux.find('o') + 1;

        for (int i = 0; i < num_saltos; i++) {
            int posicao_anterior = 0;

            if (i == 0) {
                posicao_anterior = posicao_bola_original;
            } else {
                posicao_anterior = posicao_bola[i-1];
            }
            int posicao_atual = posicao_bola[i];

            int inicio = posicao_anterior < posicao_atual ? posicao_anterior : posicao_atual;
            int fim = posicao_anterior < posicao_atual ? posicao_atual : posicao_anterior;
            for (int j = inicio + 1; j < fim; j++) {
                campo_aux[j-1] = '.';
            }
        }

        int pos_final = posicao_bola[num_saltos - 1];

        if (pos_final == 0) {
            for (int i = 0; i < (int) campo_aux.size(); i++) {
                campo_aux[i] = '.';
            }
            return "d" + campo_aux;
        }
        if (pos_final == (int) campo_aux.size()+1) {
            for (int i = 0; i < (int) campo_aux.size(); i++) {
                campo_aux[i] = '.';
            }
            return "e" + campo_aux;
        }

        campo_aux[posicao_bola_original-1] = '.';
        campo_aux[pos_final-1] = 'o';
    }


    return campo_aux;
}

bool fezGol (string campo) {
    return campo[0] == 'd' || campo[0] == 'e';
}

int minimax (JogadaT *jogada, int profundidade, bool max) {
    
    if (profundidade == 0 || fezGol(jogada->campo)) {
        return get_heuristica(jogada->lado_meu, jogada->tam_campo, jogada->campo) + profundidade;
        
    } 
    vector<string> possibilidades_jogada = geraJogadasPossiveis(jogada);
    for (auto possibilidade_jogada : possibilidades_jogada) {
        JogadaT *jogada_possivel = new JogadaT;
        jogada_possivel->lado_meu = jogada->lado_meu;
        jogada_possivel->tam_campo = jogada->tam_campo;
        jogada_possivel->campo = atualizaCampo(jogada->campo, possibilidade_jogada);
        
        jogada->jogadas_possiveis.push_back(jogada_possivel);
    }

    vector<int> valores;
    int valor; 
    if (max) {
        // -INFINITY
        valor = -std::numeric_limits<int>::max();
        for (auto jogada_possivel : jogada->jogadas_possiveis) {
            valor = std::max(valor, minimax(jogada_possivel, profundidade-1, !max));
        }
    } else {
        // INFINITY
        valor = std::numeric_limits<int>::max();
        for (auto jogada_possivel : jogada->jogadas_possiveis) {
            fflush(stdout);
            valor = std::min(valor, minimax(jogada_possivel, profundidade-1, !max));
        }
    }
     
    return valor;
}

string calculaResposta (char buf[], JogadaT *jogada) {
    string resposta;

    vector<string> jogadas_possiveis = geraJogadasPossiveis(jogada);
    cout << "\n\n=============\n";
    cout << "RECEBEU: ";
    cout << jogada->campo;
    cout << "\n";
    vector<int> resultados_heuristica;
    for (auto jogada_possivel : jogadas_possiveis) {
        JogadaT *possibilidade_jogada = new JogadaT;
        possibilidade_jogada->lado_meu = jogada->lado_meu;
        possibilidade_jogada->tam_campo = jogada->tam_campo;
        possibilidade_jogada->campo = atualizaCampo(jogada->campo, jogada_possivel);
        


        int resultado_heuristica = minimax(possibilidade_jogada, 3, false);
        resultados_heuristica.push_back(resultado_heuristica);

        cout << "\n";
        cout << possibilidade_jogada->campo;
        cout << "      Jogada resultou em ";
        cout << resultado_heuristica;
        
    }
    int index_max = max_element(resultados_heuristica.begin(), resultados_heuristica.end()) - resultados_heuristica.begin();

    cout << "\n";
    cout << jogadas_possiveis[index_max];
    cout << "  ";
    cout << atualizaCampo(jogada->campo, jogadas_possiveis[index_max]);
    cout << "      << Jogada escolhida. Resultou em ";
    cout << (int) resultados_heuristica[index_max];
    fflush(stdout);
    return jogadas_possiveis[index_max];
}

bool acabou (string campo) {
    for (int i = 0; i < campo.size(); i++) {
        if (campo[i] != '.') {
            return false;
        }
    }
    return true;
}

int main (int argc, char **argv) {
    campo_conecta(argc, argv);

    char buf[MAXSTR];

    while (1) {
        campo_recebe(buf);


        JogadaT* jogada = leJogada(buf);
        if (acabou(jogada->campo)) {
            break;
        }
        string resposta = calculaResposta(buf, jogada);
        printf("\n%s", resposta.c_str());
        fflush(stdout);

        sprintf(buf, "%s\n", resposta.c_str());
        campo_envia(buf);
    }
}