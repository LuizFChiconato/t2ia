#include <string>
using namespace std;

int calcula_fator_distancia(string campo, int posicao_bola);
int filosofos_depois(string campo, int posicao_bola);
int filosofos_antes(string campo, int posicao_bola);
int filosofos_colados_depois(string campo, int posicao_bola);
int filosofos_colados_antes(string campo, int posicao_bola);
int get_heuristica(char lado, int tamanho_campo, string campo_entrada);