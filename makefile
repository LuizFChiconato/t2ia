CFLAGS = -g
LDLIBS = -l hiredis -l readline

all: phutball

exec: all
	./phutball e

phutball:
	gcc $(CFLAGS) $(LDLIBS) -c -o campo.o campo.c 
	g++ -c -o heuristica.o heuristica.cpp $(CFLAGS) $(LDLIBS)
	g++ -c -o phutball.o phutball.cpp $(CFLAGS) $(LDLIBS)
	g++ -o phutball campo.o phutball.o heuristica.o $(CFLAGS) $(LDLIBS)

clean:
	rm -f phutball *.o