#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
using namespace std;

int calcula_fator_distancia(string campo, int posicao_bola){
    return posicao_bola - ceil((int) campo.size()/2);
}
int filosofos_depois(string campo, int posicao_bola){
    int n_filosofos = 0;
    for (int i = (int) campo.find("o"); i < (int) campo.size(); i++){
        if(campo[i] == 'f'){
            n_filosofos++;
        }
    }
    return n_filosofos;    
}
int filosofos_antes(string campo, int posicao_bola){
    int n_filosofos = 0;
    for (int i = 0; i < (int) campo.find("o"); i++){
        if(campo[i] == 'f'){
            n_filosofos++;
        }
    }
    return n_filosofos;   
}

int filosofos_colados_depois(string campo, int posicao_bola){
    int n_filosofos = 0;
    for (int i = (int) campo.find("o") + 1; i < (int) campo.size(); i++){
        if(campo[i] != 'f'){
            break;
        }
        n_filosofos++;
    }
    return n_filosofos;    
}

int filosofos_colados_antes(string campo, int posicao_bola){
    int n_filosofos = 0;
    for (int i = (int) campo.find("o") - 1; i > 0; i--){
        if(campo[i] != 'f'){
            break;
        }
        n_filosofos++;
    }
    return n_filosofos;
}

int maior_salto_favor(string campo, int tam_campo) {
    int index = campo.find("o");

    while (true) {
        if (index > tam_campo) {
            return 0;
        } else if (campo[index] == '.' && (campo[index-1] == '.' || campo[index-1] == 'o')) {
            return index - ceil((int) campo.size()/2);
        }
        index++;
    }
    return index;
}

int maior_salto_contra(string campo, int tam_campo) {
    int index = campo.find("o");

    while (true) {
        if (index == 0) {
            return 0;
        } else if (campo[index] == '.' && (campo[index+1] == '.' || campo[index+1] == 'o')) {
            return ceil((int) campo.size()/2) - index;
        }
        index--;
    }
    return index;
}

int get_heuristica(char lado, int tamanho_campo, string campo_entrada){
    // se jogo terminou
    if (campo_entrada[0] == 'd' || campo_entrada[0] == 'e') {
        if (campo_entrada[0] == lado) {
            return 1000;
        } else {
            return -1000;
        }
    }

    int posicao_bola, fator_distancia_gol, fator_n_filosofos, fator_filosofos_colados, fator_maior_salto, heuristica;
    string campo;     
    campo = "c";
    string s_o = "o";
    campo.append(campo_entrada);
    campo.append("g");
    if(lado == 'd'){
        reverse(campo.begin(), campo.end());
    }
    if (campo[campo.size()-2] == 'o'){
        return -2000;
    }
    posicao_bola = campo.find(s_o);
    //################fatores de cálculo######################
    fator_distancia_gol = calcula_fator_distancia(campo, posicao_bola);
    fator_n_filosofos = (filosofos_depois(campo, posicao_bola) - filosofos_antes(campo, posicao_bola));
    fator_filosofos_colados = (filosofos_colados_depois(campo, posicao_bola) - filosofos_colados_antes(campo, posicao_bola)) * 3;
    fator_maior_salto = (maior_salto_favor(campo, tamanho_campo) - maior_salto_contra(campo, tamanho_campo) * 2);
    heuristica = fator_distancia_gol + fator_filosofos_colados + fator_maior_salto + fator_n_filosofos ;
    
    return heuristica;
}